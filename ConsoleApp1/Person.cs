﻿using System;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace ConsoleApp1
{
    public class Person : IOracleCustomType
    {
        [OracleObjectMappingAttribute("NAME")]
        public string Name { get; set; }
        [OracleObjectMappingAttribute("PHONE")]
        public string Phone { get; set; }

        public Person()
        {
            Name = string.Empty;
            Phone = string.Empty;
        }

        public void FromCustomObject(OracleConnection con, IntPtr pUdt)
        {
            OracleUdt.SetValue(con, pUdt, "NAME", this.Name);
            OracleUdt.SetValue(con, pUdt, "PHONE", this.Phone);
        }

        public void ToCustomObject(OracleConnection con, IntPtr pUdt)
        {
            this.Name = ((string)(OracleUdt.GetValue(con, pUdt, "NAME")));
            this.Phone = ((string)(OracleUdt.GetValue(con, pUdt, "PHONE")));
        }

        public override string ToString()
        {
            return string.Format("Name={0}, Phone={1}", Name, Phone);
        }
    }
}
