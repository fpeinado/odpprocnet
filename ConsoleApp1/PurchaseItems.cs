﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class PurchaseItems : List<PurchaseItem>, IOracleCustomType
    {
        [OracleArrayMapping()]
        public object[] Values;

        public void FromCustomObject(OracleConnection con, IntPtr pUdt)
        {
            OracleUdt.SetValue(con, pUdt, 0, Values);
        }

        public void ToCustomObject(OracleConnection con, IntPtr pUdt)
        {
            Values = (object[])OracleUdt.GetValue(con, pUdt, 0);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("{");
            foreach(PurchaseItem item in this)
            {
                sb.Append(item);
            }
            sb.Append("}");

            return sb.ToString();
        }
    }
}
