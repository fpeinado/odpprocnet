﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Persons : List<Person>, IOracleCustomType
    {
        [OracleArrayMapping()]
        public object[] Values;

        public void FromCustomObject(OracleConnection con, IntPtr pUdt)
        {
            OracleUdt.SetValue(con, pUdt, 0, Values);
        }

        public void ToCustomObject(OracleConnection con, IntPtr pUdt)
        {
            Values = (object[])OracleUdt.GetValue(con, pUdt, 0);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{");
            int contador = 0;
            foreach (Person person in this)
            {
                sb.Append(" ");
                sb.Append(person);
                if (++contador < this.Count)
                {
                    sb.Append(",");
                }
            }
            sb.Append("}");
            return sb.ToString();
        }
    }
}
