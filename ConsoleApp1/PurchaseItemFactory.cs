﻿using Oracle.DataAccess.Types;

namespace ConsoleApp1
{
    [OracleCustomTypeMappingAttribute("SCOTT.LINEITEM")]
    class PurchaseItemFactory : IOracleCustomTypeFactory
    {
        public IOracleCustomType CreateObject()
        {
            return new PurchaseItem();
        }
    }
}
