﻿using Oracle.DataAccess.Client;
using System;
using System.Data;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            using (OracleConnection cnn = new OracleConnection())
            {
                cnn.ConnectionString = "Data Source=ORCL;User Id=SYSTEM;Password=Password";
                cnn.Open();
                OracleCommand cmd = cnn.CreateCommand();
                cmd.CommandText = "ALTER SESSION SET CURRENT_SCHEMA=SCOTT";
                cmd.ExecuteNonQuery();

                cmd = cnn.CreateCommand();
                cmd.CommandText = "SCOTT.P_TESTE";
                cmd.CommandType = CommandType.StoredProcedure;

                OracleParameter par = new OracleParameter();
                par.OracleDbType = OracleDbType.Object;
                par.Direction = ParameterDirection.Output;
                par.ParameterName = "person";
                par.UdtTypeName = "EXTERNAL_PERSON";
                par.Value = null;

                cmd.Parameters.Add(par);

                cmd.ExecuteNonQuery();

                Person person = (Person)cmd.Parameters["person"].Value;

                Console.WriteLine(person);

                cmd = cnn.CreateCommand();
                cmd.CommandText = "SCOTT.P_TESTE_TABLE";
                cmd.CommandType = CommandType.StoredProcedure;

                par = new OracleParameter();
                par.OracleDbType = OracleDbType.Object;
                par.Direction = ParameterDirection.Output;
                par.ParameterName = "persons";
                par.UdtTypeName = "EXTERNAL_PERSON_TABLE";
                par.Value = null;

                cmd.Parameters.Add(par);

                cmd.ExecuteNonQuery();

                Persons persons = new Persons();
                persons.AddRange((Person[])cmd.Parameters["persons"].Value);

                Console.WriteLine(persons);

                cmd = cnn.CreateCommand();
                cmd.CommandText = "SCOTT.P_PURCHASE_ORDER";
                cmd.CommandType = CommandType.StoredProcedure;

                par = new OracleParameter();
                par.OracleDbType = OracleDbType.Object;
                par.Direction = ParameterDirection.Output;
                par.ParameterName = "purchase";
                par.UdtTypeName = "PURCHASE_ORDER";
                par.Value = null;

                cmd.Parameters.Add(par);

                cmd.ExecuteNonQuery();

                PurchaseOrder po = (PurchaseOrder)cmd.Parameters["purchase"].Value;

                Console.WriteLine(po);

                Console.ReadKey();
            }
        }
    }
}
