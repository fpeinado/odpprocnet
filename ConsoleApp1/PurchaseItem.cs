﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;

namespace ConsoleApp1
{
    public class PurchaseItem : IOracleCustomType
    {
        [OracleObjectMappingAttribute("ITEM_NAME")]
        public string item_name { get; set; }
        [OracleObjectMappingAttribute("QUANTITY")]
        public decimal quantity { get; set; }
        [OracleObjectMappingAttribute("UNIT_PRICE")]
        public decimal unit_price { get; set; }

        public void FromCustomObject(OracleConnection con, IntPtr pUdt)
        {
            OracleUdt.SetValue(con, pUdt, "ITEM_NAME", this.item_name);
            OracleUdt.SetValue(con, pUdt, "QUANTITY", this.quantity);
            OracleUdt.SetValue(con, pUdt, "UNIT_PRICE", this.unit_price);
        }

        public void ToCustomObject(OracleConnection con, IntPtr pUdt)
        {
            this.item_name = ((string)(OracleUdt.GetValue(con, pUdt, "ITEM_NAME")));
            this.quantity = ((decimal)(OracleUdt.GetValue(con, pUdt, "QUANTITY")));
            this.unit_price = ((decimal)(OracleUdt.GetValue(con, pUdt, "UNIT_PRICE")));
        }

        public override string ToString()
        {
            return string.Format("item_name={0}, quantity={1}, unit_price={2}", item_name, quantity, unit_price);
        }
    }
}
