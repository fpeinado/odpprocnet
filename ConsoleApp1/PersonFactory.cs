﻿using Oracle.DataAccess.Types;

namespace ConsoleApp1
{
    [OracleCustomTypeMappingAttribute("SCOTT.EXTERNAL_PERSON")]
    public class PersonFactory : IOracleCustomTypeFactory
    {
        public IOracleCustomType CreateObject()
        {
            Person person = new Person();
            return person;
        }
    }
}
