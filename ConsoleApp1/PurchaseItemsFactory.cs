﻿using Oracle.DataAccess.Types;
using System;

namespace ConsoleApp1
{
    [OracleCustomTypeMappingAttribute("SCOTT.LINEITEM_TABLE")]
    public class PurchaseItemsFactory : IOracleArrayTypeFactory
    {
        public Array CreateArray(int numElems)
        {
            PurchaseItems items = new PurchaseItems();
            for (int x = 0; x < numElems; x++)
            {
                items.Add(new PurchaseItem());
            }
            return items.ToArray();
        }

        public Array CreateStatusArray(int numElems)
        {
            return new OracleUdtStatus[numElems];
        }
    }
}
