﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class PurchaseOrder : IOracleCustomType
    {
        [OracleObjectMappingAttribute("ID")]
        public int Id { get; set; }
        [OracleObjectMappingAttribute("CONTACT")]
        public Person Contact { get; set; }
        [OracleObjectMappingAttribute("LINEITEMS")]
        public PurchaseItems Items { get; set; }

        public void FromCustomObject(OracleConnection con, IntPtr pUdt)
        {
            OracleUdt.SetValue(con, pUdt, "ID", this.Id);
            OracleUdt.SetValue(con, pUdt, "CONTACT", this.Contact);
            OracleUdt.SetValue(con, pUdt, "LINEITEMS", this.Items);
        }

        public void ToCustomObject(OracleConnection con, IntPtr pUdt)
        {
            this.Id = ((int)(OracleUdt.GetValue(con, pUdt, "ID")));
            this.Contact = ((Person)(OracleUdt.GetValue(con, pUdt, "CONTACT")));
            this.Items = new PurchaseItems();
            Items.AddRange(((PurchaseItem[])(OracleUdt.GetValue(con, pUdt, "LINEITEMS"))));
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("{ id=");
            sb.Append(Id);
            sb.Append(", contact=");
            sb.Append(Contact.ToString());
            sb.Append(", itens=");
            sb.Append(Items.ToString());
            sb.Append("}");
            return sb.ToString();
        }
    }
}
