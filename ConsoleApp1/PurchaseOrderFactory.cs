﻿using Oracle.DataAccess.Types;

namespace ConsoleApp1
{
    [OracleCustomTypeMappingAttribute("SCOTT.PURCHASE_ORDER")]
    public class PurchaseOrderFactory : IOracleCustomTypeFactory
    {
        public IOracleCustomType CreateObject()
        {
            return new PurchaseOrder();
        }
    }
}
