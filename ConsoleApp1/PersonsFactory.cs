﻿using System;
using Oracle.DataAccess.Types;

namespace ConsoleApp1
{
    [OracleCustomTypeMappingAttribute("SCOTT.EXTERNAL_PERSON_TABLE")]
    public class PersonsFactory : IOracleArrayTypeFactory
    {
        public Array CreateArray(int numElems)
        {
            Persons people = new Persons();
            for(int x = 0; x < numElems; x++)
            {
                people.Add(new Person());
            }
            return people.ToArray();
        }

        public Array CreateStatusArray(int numElems)
        {
            return new OracleUdtStatus[numElems];
        }
    }
}